import { UUID } from 'crypto';
import { v4 as uuidv4 } from 'uuid';
import { saveAs } from 'file-saver';
import JSZip from 'jszip';

// type ConvertTypes = 'webp' | 'pdf'; // todo

export class useConvert {
    private Images: FileList;
    public Canvas: HTMLCanvasElement | null = null;
    public ImagePreview: HTMLElement | null = null;
    public static ImageUUIDList: UUID[] = [];
    public static ImageBlobList: Blob[] = [];

    public RawImage!: HTMLImageElement;

    constructor (files: FileList, previewBlock: HTMLElement | null) {
        this.Images = files;
        this.ImagePreview = previewBlock;
    }

    createImageBlocks () {
        if (this.ImagePreview){
            this.ImagePreview.innerHTML = '';
            useConvert.ImageUUIDList = [];
            useConvert.ImageBlobList = [];
        };
    }

    prepareFile (file: File):Promise<HTMLImageElement> {

        this.createImageBlocks();

        return new Promise((resolve, reject) => {
            if (!file) {
                reject('Please select a file to convert!');
            }

            const rawImage = new Image();

            rawImage.addEventListener("load", function () {
                resolve(rawImage);
            });

            rawImage.src = URL.createObjectURL(file);
        })
    }

    prepareFiles () {
        for (let file of this.Images) {
            this.convertToWebp(file);
        }
    }

    /**
     * Converting base64 string to WEBP files
     */
    convertToWebp (files: File) {
        this.prepareFile(files)
        .then((rawImage): Promise<string> => {
            return new Promise((resolve, reject) => {
                try {
                    this.Canvas = document.createElement('canvas');

                    const ctx = this.Canvas.getContext("2d") as CanvasRenderingContext2D;

                    this.Canvas.width = rawImage.width;
                    this.Canvas.height = rawImage.height;

                    ctx.drawImage(rawImage, 0, 0);

                    this.Canvas.toBlob((blob) => {
                        resolve(URL.createObjectURL(blob as Blob));
                        useConvert.ImageBlobList.push(blob as Blob);
                    }, "image/webp");
                } catch (e) {
                    reject(e);
                }
            })
        })
        .then((imageURL): Promise<{
            imageURL: string;
            scaledImg: HTMLImageElement;
        }> => {
            return new Promise((resolve, _) => {
              let scaledImg = new Image();
        
              scaledImg.addEventListener("load", () => {
                resolve({imageURL, scaledImg});
              });
        
              scaledImg.setAttribute("src", imageURL);
            });
        })
        .then((data) => {
            if (!!!this.ImagePreview) throw new Error("Image preview block not found, please reload page");

            const imageLink = document.createElement("a");
            const fileUUID = uuidv4();
            useConvert.ImageUUIDList.push(fileUUID)
            imageLink.setAttribute("href", data.imageURL);
            imageLink.setAttribute('download', `${fileUUID}.webp`);
            imageLink.appendChild(data.scaledImg);
            
            (this.ImagePreview as HTMLElement).appendChild(imageLink);
        });
    }

    static downloadAllHandler () {
        if (useConvert.ImageBlobList.length) {
            const zip = new JSZip();
            useConvert.ImageBlobList.forEach((el, index) => {
                const blob = new Blob([el], {
                    type: "image/webp"
                });
                zip.file(`${useConvert.ImageUUIDList[index]}.webp`, blob)
            })

            zip.generateAsync({type: 'blob'})
            .then((content) =>{
                saveAs(content, 'image-archive.zip')
            })
        }
        else {
            throw Error("Image list is empty, please select the image");
        }
    }

    convertHandler () {
        return new Promise((resolve, _) => {
            this.prepareFiles();
            resolve(true)
        })
    }
}