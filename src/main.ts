import { createApp } from 'vue';
import './style.css';
import App from './App.vue';
import { createGtm } from '@gtm-support/vue-gtm';

const app = createApp(App);
app.use(
    createGtm({
        id: import.meta.env.VITE_APP_GTM_KEY
    })
);
app.mount('#app');
