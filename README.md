# Image Converter
Now converter can convert any image to `WEBP`.

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/fa56f8c85d5f4581aff811e3f0c30a7b)](https://app.codacy.com/gl/a.vahitov/image-converter/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

## Dependencies
- Node 18+

## Setup

 1. Install packages
 ```
 npm install
 ``` 
 2. Run project in dev mode
 ```
 npm run dev
 ```
 3. Build project
 ```
 npm run build
 ```